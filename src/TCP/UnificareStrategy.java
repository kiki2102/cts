package TCP;

public class UnificareStrategy {

    private Strategy strategy;
    private Object obiect;

    public UnificareStrategy(Object obiect) {
        this.obiect = obiect;
        if (obiect instanceof Apel) {
            strategy = new OperatiiApel();
        } else if (obiect instanceof Sedinta) {
            strategy = new OperatiiSedinta();
        } else if (obiect instanceof Integer) {
            strategy = new OperatiiInteger();
        }

    }

    public void procesare(Reminder rem) {
        strategy.operatieObiect(this.obiect, rem);
        System.out.println(rem.toString());
        System.out.println("-----------------------------------------");
    }

}
