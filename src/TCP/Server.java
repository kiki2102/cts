
package TCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server implements Runnable {

    Reminder r = new Reminder("Apeluri.txt", "Sedinte.txt");
    OperatiiMemento om;
    ServerSocket ss;
    Socket s;
    UnificareStrategy us;
    static Server serv;

    private Server(int port) throws IOException {
        ss = new ServerSocket(port);
        ss.setReuseAddress(true);
    }

    public static Server getInstance(int port) throws IOException {
        if (serv == null) {
            serv = new Server(port);
        }
        return serv;

    }

    @Override
    public void run() {
       
        om = new OperatiiMemento();
        try {
            om.adaugaReminder(r.getListaNote());
            //System.out.println(om.getListaReminder());
            //System.out.println("---------------------------------------------------");
        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while ((s = ss.accept()) != null) {

                try (ObjectInputStream ois = new ObjectInputStream(s.getInputStream())) {
                    Object obiect;
                    while ((obiect = ois.readObject()) != null) {
                        us = new UnificareStrategy(obiect);
                        us.procesare(r);
                    }
                }

                r.scriereFisierApel("Apeluri.txt");
                r.scriereFisierSedinta("Sedinte.txt");
                System.out.println("---------------------------------------------------");

            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            if (this.ss != null) {
                try {
                    this.ss.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void main(String[] args) {
        try {

            Thread t = new Thread(Server.getInstance(8099));
            t.start();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
