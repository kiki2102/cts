package TCP;

public class OperatiiInteger implements Strategy {

    @Override
    public void operatieObiect(Object obiect, Reminder reminder) {
        reminder.informareOperativa((int) obiect);
    }

}
