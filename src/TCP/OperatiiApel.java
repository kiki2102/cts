package TCP;

public class OperatiiApel implements Strategy {

    @Override
    public void operatieObiect(Object obiect, Reminder reminder) {

        reminder.getListaNote().add((Apel) obiect);

    }

}
