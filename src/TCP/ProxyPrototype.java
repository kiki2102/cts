package TCP;

public class ProxyPrototype implements GenerareActivitate {

    private Prototype prototip;

    @Override
    public Activitate generare(String tipActivitate) {
        if (prototip == null) {
            prototip = new Prototype();
        }
        return (Activitate) prototip.getPrototip(tipActivitate);
    }

}
