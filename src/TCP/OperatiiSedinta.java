package TCP;

public class OperatiiSedinta implements Strategy {

    @Override
    public void operatieObiect(Object obiect, Reminder reminder) {
        reminder.getListaNote().add((Sedinta) obiect);
    }

}
