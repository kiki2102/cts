package TCP;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TemplateApel extends Template {

    @Override
    File deschidereFisier(String caleFisier) {
        return new File(caleFisier);
    }

    @Override
    void citireFisier(File fisier, List<Nota> listaNote) {
        if (fisier.exists() && fisier.canRead() && fisier.isFile()) {
            try {
                Scanner s = new Scanner(fisier);
                while (s.hasNextLine()) {
                    String linie = s.nextLine();
                    String[] elemente = linie.split("#");
                    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    Date data = dateFormat.parse(elemente[0]);
                    if (data != null && elemente[1] != null && elemente[2] != null) {
                        listaNote.add(new Apel(elemente[1], elemente[2], data));
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TemplateSedinta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(TemplateSedinta.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
