package TCP;

public abstract class ProgramareDecorator implements Programare {

    protected Programare programareDecorata;

    public ProgramareDecorator(Programare programareDecorata) {
        this.programareDecorata = programareDecorata;
    }

    public void programareSedinta() {
        programareDecorata.programareSedinta();
    }

}
