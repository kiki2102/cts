package TCP;

import java.io.Serializable;
import java.util.Date;

public abstract class Nota implements Serializable {

    protected Date data;

    public Nota(Date data) {
        if (data != null) {
            this.data = data;
        } else {
            throw new NullPointerException("Nu se poate apela constructorul cu null");
        }

    }

    public Date getData() {
        if (data != null) {
            return data;
        } else {
            throw new NullPointerException("Nu se poate apela getData pt ca nu exista data");
        }
    }

    public void setData(Date data) {
        if (data != null) {
            this.data = data;
        } else {
            throw new NullPointerException("Nu se poate apela setData cu null.");
        }
    }

    @Override
    public String toString() {
        if (data != null) {
            return "Nota{" + "data=" + data + '}';
        } else {
            throw new NullPointerException("Nu se poate apela toString() pt ca nu exista data");
        }
    }

    @Override
    protected abstract Object clone() throws CloneNotSupportedException, NullPointerException;

}
