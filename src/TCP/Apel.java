package TCP;

import java.util.Date;

public class Apel extends Nota implements Activitate {

    private String nrEmitator;
    private String nrReceptor;

    public Apel(String nrEmitator, String nrReceptor, Date data) {
        super(data);
        if (nrEmitator == null || nrEmitator.equals("") || nrReceptor == null || nrReceptor.equals("")) {
            throw new NullPointerException("Numarul apelantului si/sau al apelantului nu poate fi null");
        } else {
            this.nrEmitator = nrEmitator;
            this.nrReceptor = nrReceptor;
        }
    }

    public String getNrEmitator() {
        if (this.nrEmitator == null || this.nrEmitator.equals("")) {
            throw new NullPointerException("Nu exista numarul apelantului");
        } else {
            return nrEmitator;
        }
    }

    public void setNrEmitator(String nrEmitator) {
        if (this.nrEmitator == null || this.nrEmitator.equals("")) {
            throw new NullPointerException("Nu se poate apela setNrEmitator(null)");
        } else {
            this.nrEmitator = nrEmitator;
        }
    }

    public String getNrReceptor() {
        if (this.nrReceptor == null || this.nrReceptor.equals("")) {
            throw new NullPointerException("Nu exista numarul apelatului");
        } else {
            return nrReceptor;
        }
    }

    public void setNrReceptor(String nrReceptor) {
        if (this.nrReceptor == null || this.nrReceptor.equals("")) {
            throw new NullPointerException("Nu se poate apela setNrReceptor(null)");
        } else {
            this.nrReceptor = nrReceptor;
        }
    }

    @Override
    public String toString() {
        if (nrEmitator == null || nrEmitator.equals("") || nrReceptor == null || nrReceptor.equals("")) {
            throw new NullPointerException("Nu exista numarul apelantului si/sau al apelatului");
        } else {
            return super.toString() + "\n" + "Apel{" + "nrEmitator=" + nrEmitator + ", nrReceptor=" + nrReceptor + '}';
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException, NullPointerException {
        if (nrEmitator == null || nrEmitator.equals("") || nrReceptor == null || nrReceptor.equals("") || this.data == null) {
            throw new NullPointerException("Nu se poate apela clone cu unul sau mai multi parametri null");
        } else {
            Apel a = new Apel(this.nrEmitator, this.nrReceptor, this.data);
            return a;
        }
    }

    @Override
    public void detaliiActivitate() {
        if (nrEmitator == null || nrEmitator.equals("") || nrReceptor == null || nrReceptor.equals("") || this.data == null) {
            throw new NullPointerException();
        } else {
            System.out.println("Apelul facut de numarul " + this.nrEmitator + " catre numarul "
                    + nrReceptor + " a fost plasat la data de " + this.data);
        }
    }

}
