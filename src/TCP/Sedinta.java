package TCP;

import java.util.Date;

public class Sedinta extends Nota implements Programare, Activitate {

    private String subiect;

    public Sedinta(String subiect, Date data) {
        super(data);
        if (subiect == null || subiect.equals("")) {
            throw new NullPointerException("Subiectul sedintei nu poate fi null");
        } else {
            this.subiect = subiect;
        }
    }

    public String getSubiect() {
        if (subiect == null) {
            throw new NullPointerException("Nu exista subiectul sedintei");
        } else {
            return subiect;
        }
    }

    public void setSubiect(String subiect) {
        if (subiect == null || subiect.equals("")) {
            throw new NullPointerException("Nu se poate seta subiectul sedintei null");
        } else {
            this.subiect = subiect;
        }
    }

    @Override
    public String toString() {
        if (this.subiect.equals("") || this.subiect == null || this.data == null) {
            throw new NullPointerException("Subiectul si/sau data sedintei nu exista.");
        } else {
            return super.toString() + System.lineSeparator() + "Sedinta{" + "subiect=" + subiect + '}';
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        if (this.subiect.equals("") || this.subiect == null || this.data == null) {
            throw new NullPointerException("Trebuie sa existe argumente nenule.");
        } else {
            Sedinta s = new Sedinta(this.subiect, this.data);
            return s;
        }

    }

    @Override
    public void programareSedinta() {
        if (this.subiect == null || this.subiect.equals("") || this.data == null) {
            throw new NullPointerException("Sedinta nu are data si/sau subiect");
        } else {
            System.out.println("Se doreste programarea unei sedinte la data de:"
                    + this.data + " cu titlul:" + this.subiect);
        }
    }

    @Override
    public void detaliiActivitate() {
        if (this.subiect == null || this.subiect.equals("") || this.data == null) {
            throw new NullPointerException("Sedinta nu are data si/sau subiect");
        } else {
            System.out.println("Sedinta cu subiectul " + this.subiect + " va avea loc in data de "
                    + this.data);
        }
    }

}
