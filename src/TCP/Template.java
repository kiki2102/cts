package TCP;

import java.io.File;
import java.util.List;

public abstract class Template {

    abstract File deschidereFisier(String caleFisier);

    abstract void citireFisier(File fisier, List<Nota> listaNote);

    public final void citire(String caleFisier, List<Nota> listaNote) {

        File f = deschidereFisier(caleFisier);
        citireFisier(f, listaNote);

    }

}
