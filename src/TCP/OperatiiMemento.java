package TCP;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class OperatiiMemento {

    private List<List<Nota>> listaReminder = new LinkedList<>();

    public List<List<Nota>> getListaReminder() {
        return listaReminder;
    }

    public int getNrReminder() {
        return listaReminder.size();
    }

    public void adaugaReminder(List<Nota> r) throws Exception {
        if (r != null) {
            listaReminder.add(new ArrayList<>(r));
        } else {
            throw new NullPointerException("Reminder nu poate fi null");
        }
    }

    public List<Nota> getReminder(Integer moment) throws Exception {

        if (moment == null || moment < 0 || moment >= listaReminder.size()) {
            throw new Exception("Moment invalid");

        } else {
            return listaReminder.get(moment);
        }
    }

}
