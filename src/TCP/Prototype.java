package TCP;

import java.util.Date;

public class Prototype {

    Apel apel;
    Sedinta sedinta;
    final Long delay = 200000L;

    public Prototype() {
        this.apel = new Apel("07xxxxxxxx", "07xxxxxxxx", new Date(System.currentTimeMillis() + delay));
        this.sedinta = new Sedinta("SubiectSedinta", new Date(System.currentTimeMillis() + delay));
    }

    public Nota getPrototip(String tip) {
        try {
            if (tip.equalsIgnoreCase("APEL")) {
                return (Nota) apel.clone();
            } else if (tip.equalsIgnoreCase("SEDINTA")) {
                return (Nota) sedinta.clone();
            }
        } catch (CloneNotSupportedException ex) {
            System.err.println(ex.getMessage());

        }
        return null;
    }

}
