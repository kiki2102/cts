package TCP;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    Socket s;
    Integer port;
    InetAddress adresa;
    ObjectOutputStream oos;

    public Client(Integer port) throws IOException {
        this.port = port;
        this.adresa = InetAddress.getLocalHost();
        this.initServer();
    }

    public void setAdresa(String adresa) {
        try {
            InetAddress addr = InetAddress.getByName(adresa);
            this.adresa = addr;
        } catch (Exception e) {
            System.err.println("Adresa invalida. Vom utilia localhost");
        }
    }

    private void initServer() throws IOException {
        s = new Socket(adresa, this.port);
        oos = new ObjectOutputStream(s.getOutputStream());
    }

    public void send(Object o) throws IOException {
        oos.writeObject(o);
    }

    public static void main(String[] args) {
        try {
            Client c = new Client(8099);
            Nota apelDefault = new Prototype().getPrototip("apel");
            Nota sedintaDefault = new Prototype().getPrototip("sedinta");
            System.out.println(apelDefault);
            System.out.println(sedintaDefault);

            c.send(apelDefault);
            c.send(sedintaDefault);
            //Programare progrImportanta;
            // progrImportanta = new ProgramareImportanta((Programare) sedintaDefault);
            // progrImportanta.programareSedinta();

            //ActivitateFactory af=new ActivitateFactory();
            //Activitate a1=af.getActivitate("APEL");
            //a1.detaliiActivitate();
            Integer n = 15;
            c.send(n);
            c.send(null);
        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
