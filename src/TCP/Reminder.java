package TCP;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Reminder {

    List<Nota> listaNote;

    Template tempSedinta;
    Template tempApel;

    private String caleFisApel;
    private String caleFisSedinta;

    public List<Nota> getListaNote() {
        if (listaNote == null) {
            throw new NullPointerException("Lista nu a fost initializata.");

        } else {
            return listaNote;
        }
    }

    public void setListaNote(List<Nota> listaNote) {
        if (listaNote == null) {
            throw new NullPointerException("Nu este permis setListaNote(null)");
        } else {
            this.listaNote = listaNote;
        }
    }

    public Reminder(String caleFisA, String caleFisS) {

        listaNote = new LinkedList<>();
        caleFisApel = caleFisA;
        caleFisSedinta = caleFisS;

        tempSedinta = new TemplateSedinta();
        tempApel = new TemplateApel();
        tempSedinta.citire(caleFisS, listaNote);
        tempApel.citire(caleFisA, listaNote);

    }

    @Override
    public String toString() {
        if (listaNote == null) {
            throw new NullPointerException("Lista nu a fost initializata.");
        } else {
            return "Reminder{" + "listaNote=" + listaNote + '}';
        }
    }

    public void informareOperativa(int n) {
        Date data = new Date();

        for (Nota nota : listaNote) {
            if (nota.getData().after(data)) {
                if (n == 0) {
                    throw new NullPointerException("Nu se poate executa informareOperativa(0)");
                } else if ((int) ((nota.getData().getTime() - data.getTime()) / (1000 * 60 * 60 * 24)) <= n) {
                    System.out.println("-----------------------------------");
                    System.out.println("Informare operativa");
                    System.out.println(nota);
                    System.out.println("-----------------------------------");
                }
            }

        }
    }

    public void scriereFisierApel(String caleFisier) {
        Date data = new Date();
        List<Apel> listaActualizataApel = new LinkedList<>();

        for (Nota a : listaNote) {
            if (a instanceof Apel) {
                if (a.getData().after(data)) {
                    listaActualizataApel.add((Apel) a);
                }
            }

        }
        StringBuilder sb = new StringBuilder();

        for (Apel a : listaActualizataApel) {
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            sb.append(df.format(a.getData()) + "#" + a.getNrEmitator() + "#" + a.getNrReceptor());
            sb.append(System.getProperty("line.separator"));
        }

        try {
            Path fisier = Paths.get(caleFisier);
            Files.write(fisier, sb.toString().getBytes());
        } catch (IOException ex) {
            Logger.getLogger(Reminder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException n) {
            Logger.getLogger(Reminder.class.getName()).log(Level.SEVERE, null, n);
        }

    }

    public void scriereFisierSedinta(String caleFisier) {
        Date data = new Date();
        List<Sedinta> listaActualizataSedinta = new LinkedList<>();

        for (Nota s : listaNote) {
            if (s instanceof Sedinta) {
                if (s.getData().after(data)) {
                    listaActualizataSedinta.add((Sedinta) s);
                }
            }

        }
        StringBuilder sb = new StringBuilder();

        for (Sedinta s : listaActualizataSedinta) {
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

            sb.append(df.format(s.getData()) + "#" + s.getSubiect());
            sb.append(System.getProperty("line.separator"));
        }

        try {
            Path fisier = Paths.get(caleFisier);
            Files.write(fisier, sb.toString().getBytes());
        } catch (IOException ex) {
            Logger.getLogger(Reminder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException n) {
            Logger.getLogger(Reminder.class.getName()).log(Level.SEVERE, null, n);
        }

    }

}
