package TCP;

public class ProgramareImportanta extends ProgramareDecorator {

    public ProgramareImportanta(Programare programareDecorata) {
        super(programareDecorata);
    }

    @Override
    public void programareSedinta() {

        super.programareSedinta();
        setareGradImportanta();
    }

    public void setareGradImportanta() {
        System.out.println("Se efectueaza programarea unei sedinte imporante.");
    }
}
