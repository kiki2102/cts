package TCP;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TemplateSedinta extends Template {

    @Override
    File deschidereFisier(String caleFisier) {
        if (caleFisier == null || caleFisier.equals("")) {

            throw new NullPointerException("Nu se poate  deschide un fisier inexistent");

        } else {
            return new File(caleFisier);
        }

    }

    @Override
    void citireFisier(File fisier, List<Nota> listaNote) {
        if (fisier.exists() && fisier.canRead() && fisier.isFile()) {
            try {
                Scanner s = new Scanner(fisier);
                while (s.hasNextLine()) {
                    String linie = s.nextLine();
                    String[] elemente = linie.split("#");
                    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    Date data = dateFormat.parse(elemente[0]);
                    if (data != null && elemente[1] != null) {
                        listaNote.add(new Sedinta(elemente[1], data));
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TemplateSedinta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(TemplateSedinta.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

}
