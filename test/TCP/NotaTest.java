package TCP;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class NotaTest {

    public NotaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getData method, of class Nota.
     */
    @Test
    public void testGetData() {
        System.out.println("getData");
        Nota instance = null;
        Date expResult = null;
        try {
            Date result = instance.getData();
            fail("Ar trebui sa arunce NullPointerException");
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    /**
     * Test of setData method, of class Nota.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");
        Date data = null;
        Nota instance = null;
        try {
            instance.setData(data);
            fail("Ar trebui sa arunce NullPointerException.");
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    /**
     * Test of toString method, of class Nota.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Nota instance = null;

        try {
            String result = instance.toString();
            fail("Ar trebui sa arunce NullPointerException");
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    /**
     * Test of clone method, of class Nota.
     */
    @Test
    public void testClone() throws Exception {
        System.out.println("clone");
        Nota instance = null;
        Object expResult = null;
        try {
            Object result = instance.clone();
            fail("Ar trebui sa arunce NullPointerException");
        } catch (Exception ex) {
            ex.toString();
        }

    }

    public class NotaImpl extends Nota {

        public NotaImpl() {
            super(null);
        }

        public Object clone() throws CloneNotSupportedException, NullPointerException {
            return null;
        }
    }

}
