package TCP;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TCP.ApelTest.class, TCP.NotaTest.class, TCP.ReminderTest.class, TCP.TemplateSedintaTest.class, TCP.SedintaTest.class, TCP.ActivitateFactoryTest.class})
public class testSuite {

    public static void main(String[] args) {

        Result r = JUnitCore.runClasses(testSuite.class);
        System.out.println(r.wasSuccessful());
        for (Failure f : r.getFailures()) {
            System.out.println(f);
        }
    }
}
